﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class Admob : MonoBehaviour {

	private float timeInterstitialShowed;
	InterstitialAd interstitial;
	public string nextSceneName;
	bool isShowing;
	bool isShowingReload;

	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-7003710056124472/4399482742";
		#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7003710056124472/7352949145";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
		interstitial.OnAdClosed += HandleInterstitialClosed;
	}
	
	// Update is called once per frame
	void Update () {
		if (interstitial.IsLoaded() && gameObject.GetComponent<BoxCollider2D>().enabled == true) {
			interstitial.Show();
			timeInterstitialShowed = Time.time;
			isShowing = true;
			gameObject.GetComponent<BoxCollider2D>().enabled  = false;
		}else if(!interstitial.IsLoaded() && gameObject.GetComponent<BoxCollider2D>().enabled == true){
			Application.LoadLevel(nextSceneName);
		}
		if(((Time.time - timeInterstitialShowed) > 5 && isShowing)){
			interstitial.Destroy();
			Application.LoadLevel(nextSceneName);
		}

		if (interstitial.IsLoaded() && gameObject.GetComponent<CircleCollider2D>().enabled == true) {
			interstitial.Show();
			timeInterstitialShowed = Time.time;
			isShowingReload = true;
			gameObject.GetComponent<CircleCollider2D>().enabled  = false;
		}else if(!interstitial.IsLoaded() && gameObject.GetComponent<CircleCollider2D>().enabled == true){
			Application.LoadLevel(Application.loadedLevel);
		}

		if((Time.time - timeInterstitialShowed) > 5 && isShowingReload){
			interstitial.Destroy();
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		if (!isShowingReload) {
			interstitial.Destroy ();
			Application.LoadLevel (nextSceneName);
		} else {
			interstitial.Destroy ();
			Application.LoadLevel(Application.loadedLevel);
		}
	}

}
