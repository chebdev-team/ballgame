﻿#pragma strict

public var isGravity : boolean;
public var isFix : boolean;
var ball : GameObject;
var object1 : GameObject;
var object2 : GameObject;
var object4 : GameObject;
private var Xball : float;
private var Yball : float;
private var Xobject1 : float;
private var Yobject1 : float;
private var Xobject2 : float;
private var Yobject2 : float;
private var Xobject4 : float;
private var Yobject4 : float;
private var angleObject1 : float;
private var angleObject2 : float;
private var angleObject4 : float;
private var durakSec : int;
private var buttonSize : float;
private var isPaused : boolean;
var objectWithRotation1: GameObject;
var gravityScaleObj1 : float;
var gravityScaleObj2 : float;

function Start() {
    Xball = GameObject.FindWithTag("Player").transform.position.x;
    Yball = GameObject.FindWithTag("Player").transform.position.y;

}

function OnGUI() {
    if (isGravity) {
        GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 1;
        durakSec = 1;
        object1.GetComponent.<Rigidbody2D>().gravityScale = gravityScaleObj1;
        object1.GetComponent.<Rigidbody2D>().isKinematic = false;
        ball = GameObject.FindWithTag("Player");
        ball.GetComponent.<Rigidbody2D>().isKinematic = false;
        Xobject1 = object1.transform.position.x;
        Yobject1 = object1.transform.position.y;
        Xobject2 = object2.transform.position.x;
        Yobject2 = object2.transform.position.y;
        Xobject4 = object4.transform.position.x;
        Yobject4 = object4.transform.position.y;
        angleObject1 = object1.transform.eulerAngles.z;
        angleObject2 = object2.transform.eulerAngles.z;
        angleObject4 = object4.transform.eulerAngles.z;
        objectWithRotation1.GetComponent. < Rotation > ().countDoubleClicks = 0;
        objectWithRotation1.GetComponent. < Rotation > ().gravityRotation = false;
        isGravity = false;
    }

    if (isFix) {
        GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 0;
        GameObject.FindWithTag("Player").transform.position.x = Xball;
        GameObject.FindWithTag("Player").transform.position.y = Yball;
        object1.GetComponent.<Rigidbody2D>().gravityScale = 0;
        object2.GetComponent.<Rigidbody2D>().gravityScale = 0;
        ball = GameObject.FindWithTag("Player");
        ball.GetComponent.<Rigidbody2D>().isKinematic = true;
        object1.GetComponent.<Rigidbody2D>().isKinematic = true;


        if (durakSec != 0) {
            object1.transform.position.x = Xobject1;
            object1.transform.position.y = Yobject1;
            object2.transform.position.x = Xobject2;
            object2.transform.position.y = Yobject2;
            object4.transform.position.x = Xobject4;
            object4.transform.position.y = Yobject4;
            object1.transform.eulerAngles.z = angleObject1;
            object2.transform.eulerAngles.z = angleObject2;
            object4.transform.eulerAngles.z = angleObject4;
            objectWithRotation1.GetComponent. < Rotation > ().countDoubleClicks = 0;
            objectWithRotation1.GetComponent. < Rotation > ().gravityRotation = true;
        }
        isFix = false;
    }
}