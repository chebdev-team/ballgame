﻿#pragma strict

private var myCam: Camera;
private var screenPos: Vector3;
private var angleOffset: float;
private var rotate: boolean = false;
private var isActive: boolean = false;
var objName: String;
var spriteImage1: Sprite;
var spriteImage2: Sprite;
private var v3: Vector3;
private var angle: float;
var box: GameObject;
private var point: Vector3;
private var point2: Vector3;
var isDraged: boolean = false;
var doubleClickStart : float = -1.0;
var disableClicks = false;
var isDoubleClicked = false;
var countDoubleClicks : int =0;
var gravityRotation : boolean = true;
var cameraObject : GameObject;

function Start() {
    myCam = Camera.main;
    point2 = transform.position;
}

function OnMouseDown() {

}

function OnMouseDrag() {
    point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    point.z = gameObject.transform.position.z;
    gameObject.transform.position = point;

    if ((Mathf.Abs(point.x - point2.x) > 1 && Mathf.Abs(point.y - point2.y) > 1)) {
        isDraged = true;
    }
}



function Update() {

    if (isDraged && gameObject.GetComponent.<Rigidbody2D>().IsSleeping()) {
        cameraObject.GetComponent. < Tutorial > ().tutorialDrag = true;
    }


    if (isDoubleClicked && countDoubleClicks % 2 != 0 && gravityRotation) {
        //This  only on the frame the button is clicked
        if (Input.GetMouseButtonDown(0)) {
            screenPos = myCam.WorldToScreenPoint(transform.position);
            v3 = Input.mousePosition - screenPos;
            angleOffset = (Mathf.Atan2(transform.right.y, transform.right.x) - Mathf.Atan2(v3.y, v3.x)) * Mathf.Rad2Deg;

        }
        //This  while the button is pressed down
        if (Input.GetMouseButton(0)) {
            v3 = Input.mousePosition - screenPos;

            angle = Mathf.Atan2(v3.y, v3.x) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, 0, angle + angleOffset);

        }
        GetComponent(SpriteRenderer).sprite = spriteImage2;

    }


    if (isDoubleClicked && countDoubleClicks % 2 == 0) {
        GetComponent(SpriteRenderer).sprite = spriteImage1;
        cameraObject.GetComponent. < Tutorial > ().tutorialRotate = true;
    }
}




function OnMouseUp() {
    //EDIT TO DISABLE MOUSE CLICKS FOR A TIME AFTER DOUBLE CLICK
    if (disableClicks)
        return;
    //END EDIT

    //make sure doubleClickStart isn't negative, that'll break things
    if (doubleClickStart > 0 && (Time.time - doubleClickStart) < 0.4) {
        this.OnDoubleClick();
        doubleClickStart = -1;
        lockClicks();
    } else {
        doubleClickStart = Time.time;
    }
}

//EDIT TO DISABLE MOUSE CLICKS FOR A TIME AFTER DOUBLE CLICK
function lockClicks() {
    disableClicks = true;
    yield WaitForSeconds(0.4);
    disableClicks = false;
}
//END EDIT

function OnDoubleClick() {
    // do some stuff.       
    isDoubleClicked = true;
    countDoubleClicks += 1;

}