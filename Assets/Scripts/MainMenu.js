﻿#pragma strict


var buttonSkin : GUISkin;
private var buttonSize : float;
private var i : int;
var dialogQuit : GUISkin;
var settingsSkin : GUISkin;
var settingsDisabledSkin : GUISkin;
var helpSkin : GUISkin;
var menuHelpText : GUISkin;
private var isQuite : boolean;
private var isHelp : boolean;


function OnStart(){
	
}

function Update() {
    if (Input.GetKeyDown(KeyCode.Escape)) {
        isQuite = true;
        isHelp = false;
    }

    if (PlayerPrefs.GetInt("SoundState") == 1) {
        AudioListener.volume = 100;
    } else {
        AudioListener.volume = 0;
    }
}

function OnGUI() {

    buttonSize = Screen.width / 5;
    GUI.skin = buttonSkin;
    GUI.Box(Rect(0, 0, Screen.width, Screen.height), "");
    buttonSkin.button.fontSize = Screen.width * 0.025f;
    buttonSkin.box.fontSize = Screen.width * 0.04f;
    if (GUI.Button(Rect((Screen.width / 2 - 0.5 * buttonSize), (Screen.height / 10 * 6 + Screen.height / 20), buttonSize, Screen.height / 10), "PLAY")) {
        Application.LoadLevel("scene0");
    }

    if (GUI.Button(Rect((Screen.width / 2 - 0.5 * buttonSize), (Screen.height / 10 * 7.1 + Screen.height / 20), buttonSize, Screen.height / 10), "Tutorial")) {
        Application.LoadLevel("scene21");
    }

    if (GUI.Button(Rect((Screen.width / 2 - 0.5 * buttonSize), (Screen.height / 10 * 8.2 + Screen.height / 20), buttonSize, Screen.height / 10), "Exit")) {
        isQuite = true;
    }


    if (AudioListener.volume != 0) {
        GUI.skin = settingsSkin;
        if (GUI.Button(Rect((Screen.width / 20 * 16.5), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
            PlayerPrefs.SetInt("SoundState", 0);

        }
    } else {
        GUI.skin = settingsDisabledSkin;
        if (GUI.Button(Rect((Screen.width / 20 * 16.5), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
            PlayerPrefs.SetInt("SoundState", 1);

        }
    }


    GUI.skin = helpSkin;

    if (GUI.Button(Rect((Screen.width / 20 * 18), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
        isHelp = true;
    }

    if (isHelp) {
        GUI.skin = menuHelpText;
        menuHelpText.textArea.fontSize = Screen.width * 0.027f;
        GUI.Box(Rect(0, 0, Screen.width, Screen.height), "");
        if (GUI.Button(Rect((Screen.width / 20 * 1), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
            isHelp = false;
        }


        GUI.TextArea(Rect(Screen.width / 5, Screen.height / 5, Screen.width / 5 * 3, Screen.height / 5 * 3), "\n The main task of this game is to throw the ball into the basket. To do this, you can use all objects located on the game scene. Just Drag and Drop them to the right place. Also, you can rotate some items around their axis. To do this, double tap on object and swipe on screen when round arrow will appear. To stop the rotation, select another object on the scene. \n \n GOOD LUCK!!!", 10000);
    }

    if (isQuite) {
        GUI.skin = dialogQuit;
        dialogQuit.button.fontSize = Screen.width * 0.03f;
        GUI.Box(Rect(0, 0, Screen.width, Screen.height), "");

        if (GUI.Button(Rect((Screen.width / 5) * 4 - Screen.width / 8, Screen.height / 1.8, Screen.width / 8 * 0.9, Screen.width / 8 * 0.9), "YES")) {
            Application.Quit();
        }
        if (GUI.Button(Rect(Screen.width / 5, Screen.height / 1.8, Screen.width / 8 * 0.9, Screen.width / 8 * 0.9), "NO")) {
            isQuite = false;
            Application.LoadLevel("scene00");
        }
    }


}