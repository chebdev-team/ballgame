﻿#pragma strict
var playTime : float;
var isPlayAllowed : boolean;
function OnCollisionEnter2D(coll: Collision2D) {
	if(isPlayAllowed){
    	if (coll.gameObject.tag == "CollisionSound") {
        	GetComponent.<AudioSource>().Play();
        	playTime = Time.time;
    	}
    }
}

function Update(){
	if((Time.time - playTime) > 1){
		isPlayAllowed = true;
	}else{
		isPlayAllowed = false;
	}
}

function Start(){
	isPlayAllowed = true;
}