﻿#pragma strict
import UnityEngine.UI;


public var levelScoreName : String;
public var previousLevelScoreName : String;
public var Level0Stars : Sprite;
public var Level1Stars : Sprite;
public var Level2Stars : Sprite;
public var Level3Stars : Sprite;

function Start () {
	if(!PlayerPrefs.HasKey(levelScoreName) && !PlayerPrefs.HasKey(previousLevelScoreName)){
		gameObject.GetComponent(Button).enabled = false;
		Debug.Log(gameObject.name);
		if(gameObject.name == "Level1"){
			gameObject.GetComponent(Button).image.sprite = Level0Stars;
			gameObject.GetComponent(Button).enabled = true;
		}
	} else {
		switch (PlayerPrefs.GetInt(levelScoreName)) {
		case 0 : 
			gameObject.GetComponent(Button).enabled = true;
			gameObject.GetComponent(Button).image.sprite = Level0Stars;
		break;
		case 1 : 
			gameObject.GetComponent(Button).enabled = true;
			gameObject.GetComponent(Button).image.sprite = Level1Stars;
		break;
		case 2 : 
			gameObject.GetComponent(Button).enabled = true;
			gameObject.GetComponent(Button).image.sprite = Level2Stars;
		break;
		case 3 : 
			gameObject.GetComponent(Button).enabled = true;
			gameObject.GetComponent(Button).image.sprite = Level3Stars;
		break;
		default:
		Debug.Log("no");
			gameObject.GetComponent(Button).enabled = true;
			gameObject.GetComponent(Button).image.sprite = Level0Stars;
		break;
		}
	}
}

