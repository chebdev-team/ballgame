﻿#pragma strict

private var angleObject1 : float;
private var rotAngle : float;
var objKacheli : GameObject;
private var direction : int;

function Start() {
    direction = 1;
}

function FixedUpdate() {
    rotAngle = 60 * Time.deltaTime;
    transform.Rotate(0, 0, rotAngle * direction);
    angleObject1 = objKacheli.transform.eulerAngles.z;

    if (angleObject1 > 270 && angleObject1 < 271) {
     direction = -1;
 	}
 	if (angleObject1 < 91 && angleObject1 > 90) {
     direction = 1;
 	}
}
