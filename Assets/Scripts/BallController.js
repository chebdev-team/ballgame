﻿#pragma strict
import UnityEngine.UI;

var power: int;
private
var isDone: boolean = false;
private var timeCollision: float;
private var isAllowed: boolean = true;
private var finishCollisionCount:int = 0;
private var curentLevelScore : int;
var isHowLevelCompleted : boolean;
public var PanelLevelCompleted : GameObject;
public var LevelComplitedStars : GameObject;
public var LevelComplitedStars_0 : Sprite;
public var LevelComplitedStars_1 : Sprite;
public var LevelComplitedStars_2 : Sprite;
public var LevelComplitedStars_3 : Sprite;
public var GameStars : GameObject;
public var GameStars_0 : Sprite;
public var GameStars_1 : Sprite;
public var GameStars_2 : Sprite;
public var GameStars_3 : Sprite;
var stopStarDecreasing : boolean;

/* STARS variables */
var timePassed : int;
var limit1 : int;
var limit2 : int;
var limit3 : int;
var stars = new Array(1,1,1);
var levelScore : int =0;
/* END STARS variables */

function Start() {
    gameObject.GetComponent.<Rigidbody2D>().isKinematic = true;
	curentLevelScore = PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel.ToString());
}

function Update() {
    if ((Time.time - timeCollision) > 1) {
        isAllowed = true;
    }
    timePassed = Time.timeSinceLevelLoad;
}

function OnCollisionEnter2D(coll: Collision2D) {
    if (coll.gameObject.name == "Duhovka2" || coll.gameObject.name == "level_16_porshen_verh" || coll.gameObject.name == "level_11_kacheli_osnova") {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.up * power);
    }


    if (coll.gameObject.name == "Level_1_propeller" && isAllowed) {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.up * power);
        timeCollision = Time.time;
        isAllowed = false;
    }


    if (coll.gameObject.name == "level_11_pushka") {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.right * power);
    }

    if (coll.gameObject.name == "Basket_bottom") {
        PlayerPrefs.SetInt("Level" + Application.loadedLevel.ToString(), 1);
        isDone = true;
        finishCollisionCount++;
        if (finishCollisionCount == 1){
		if(timePassed > limit3){
			levelScore = 0;
			LevelComplitedStars.GetComponent(Image).sprite = LevelComplitedStars_0;
		}else if (timePassed > limit2 && timePassed < limit3){
			levelScore = 1;
			LevelComplitedStars.GetComponent(Image).sprite = LevelComplitedStars_1;
		}else if (timePassed > limit1 && timePassed < limit2){;
			levelScore = 2;
			LevelComplitedStars.GetComponent(Image).sprite = LevelComplitedStars_2;
		}else if (timePassed < limit1){
			levelScore = 3;
			LevelComplitedStars.GetComponent(Image).sprite = LevelComplitedStars_3;
		}
        if(!PlayerPrefs.HasKey("LevelScore" + Application.loadedLevel.ToString()) || curentLevelScore < levelScore){
         PlayerPrefs.SetInt("LevelScore" + Application.loadedLevel.ToString(), levelScore);
        }
         }
       }
}

function OnGUI() {
    if (isDone) {
         	iTween.MoveTo (PanelLevelCompleted, iTween.Hash ("position", new Vector3 (0.0f, 0.0f, 0.0f), "time", 0.5,"islocal",true));
//		if(timePassed > limit3){
//			levelScore = 0;
//			
//		}else if (timePassed > limit2 && timePassed < limit3){
//			
//			levelScore = 1;
//		}else if (timePassed > limit1 && timePassed < limit2){
//			
//			levelScore = 2;
//		}else if (timePassed < limit1){
//			
//			levelScore = 3;
//		}
		isDone = false;
		stopStarDecreasing = true;
    }
    if(!stopStarDecreasing){
    	if(timePassed > limit3){
				GameStars.GetComponent(Image).sprite = GameStars_0;
		}else if (timePassed > limit2 && timePassed < limit3){
				GameStars.GetComponent(Image).sprite = GameStars_1;
		}else if (timePassed > limit1 && timePassed < limit2){
				GameStars.GetComponent(Image).sprite = GameStars_2;
		}else if (timePassed < limit1){
				GameStars.GetComponent(Image).sprite = GameStars_3;
		}
	}
}