﻿#pragma strict
public var isGravityOn : boolean;

public function Update () {
	if (Input.GetKeyDown(KeyCode.Escape)) {
		iTween.MoveTo (GameObject.Find("PausePanel"), iTween.Hash ("position", new Vector3 (0.0f, 0.0f, 0.0f), "time", 0.5,"islocal",true));
		StartCoroutine("TimeStop");
	}
}

public function HideAdditionMenu (quitPannel: GameObject){
	iTween.MoveTo (quitPannel, iTween.Hash ("position", new Vector3 (0.0f, 1200, 0.0f), "time", 0.5,"islocal",true));
}

//Кнопки на игровом меню
public function Gravity3Obj () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = true;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj).isGravity = true ;
	}
}
public function Fix3Obj () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = false;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj).isFix = true ;
	}
}


public function Gravity2x2Obj () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = true;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity2x2Obj).isGravity = true ;
	}
}
public function Fix2x2Obj () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = false;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity2x2Obj).isFix = true ;
	}
}


public function GravityDefault () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = true;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity).isGravity = true ;
	}
}
public function FixDefault () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = false;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity).isFix = true ;
	}
}


public function Gravity3Obj1Rot () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = true;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj1Rot).isGravity = true ;
	}
}
public function Fix3Obj1Rot () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = false;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj1Rot).isFix = true ;
	}
}


public function GravityObj1G1R () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = true;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj1G1R).isGravity = true ;
	}
}
public function FixObj1G1R () {
	if(Time.timeScale != 0 ){
		GameObject.Find("GameEventHandler").GetComponent(GameEventHandler).isGravityOn = false;
		GameObject.Find("Main Camera").GetComponent(OnGuiGravity3Obj1G1R).isFix = true ;
	}
}


public function Pause(pausePanel: GameObject) {
	iTween.MoveTo (pausePanel, iTween.Hash ("position", new Vector3 (0.0f, 0.0f, 0.0f), "time", 0.5,"islocal",true));
	StartCoroutine("TimeStop");
}

public function GravitySound() {
	if(Time.timeScale != 0 ){
		GetComponent.<AudioSource>().Play();
	}
}


function TimeStop(){
        yield WaitForSeconds(0.5);
        Time.timeScale = 0;
}

public function PauseToMenu() {
	Application.LoadLevel("MainMenu");
    Time.timeScale = 1;      
}

public function PauseToReload() {
 	Application.LoadLevel(Application.loadedLevel);
    Time.timeScale = 1;    
}

public function PauseToReloadAdmob() {
 	 GameObject.Find("AdmobShower").GetComponent(CircleCollider2D).enabled = true;    
    Time.timeScale = 1;    
}

public function PauseToReturn() {
    Time.timeScale = 1;    
}

public function Admob() {
    GameObject.Find("AdmobShower").GetComponent(BoxCollider2D).enabled = true;    
}


public function LoadLevel (levelName: String) {
	Application.LoadLevel(levelName);
}

public function FacebookShare() {
//* Facebook variables */
var AppId: String = "105481446453310";
var ShareUrl: String = "http://www.facebook.com/dialog/feed";
var fblink: String = "http://ya.ru";
var fbpictureLink: String = "https://lh6.googleusercontent.com/-BPw3xoPe-PY/VWC_p8Gf5dI/AAAAAAAAAPE/ZghIFMuMhzw/w1024-h500-no/1024x500.png";

#if UNITY_ANDROID
var fbcaption: String = "Find this game on Google Play http://play.google.com/hedgieball";
#elif UNITY_IPHONE
var fbcaption: String = "Find this game in AppStore https://itunes.apple.com/us/app/hedgie-ball/id996121572?mt=8";
#endif

var fbname: String = "Hedgie Ball achievement";
var fbdescription: String = "I cleared level in Hedgie Ball";
var fbredirectUri: String = "http://www.facebook.com";
/* END Facebook variables */ 
    Application.OpenURL(ShareUrl +
        "?app_id=" + AppId +
        "&link=" + WWW.EscapeURL(fblink) +
        "&picture=" + WWW.EscapeURL(fbpictureLink) +
        "&name=" + WWW.EscapeURL(fbname) +
        "&caption=" + WWW.EscapeURL(fbcaption) +
        "&description=" + WWW.EscapeURL(fbdescription) +
        "&redirect_uri=" + WWW.EscapeURL(fbredirectUri));
}

public function TwitterShare() {
//* Twitter variables */
var Address: String = "http://twitter.com/intent/tweet";
var lang: String = "en";
var text: String = "I cleared level in the Hedgie Ball";

#if UNITY_ANDROID
var url: String = "http://play.google.com/hedgieball";
#elif UNITY_IPHONE
var url: String = "https://itunes.apple.com/us/app/hedgie-ball/id996121572?mt=8";
#endif

/* END Twitter variables */
//Twitter method
   	 	Application.OpenURL(Address +
        	"?text=" + WWW.EscapeURL(text) +
        	"&url=" + WWW.EscapeURL(url) +
        "	&lang=" + WWW.EscapeURL(lang));
}

