﻿#pragma strict
import UnityEngine.UI;



public var 	buttonSoundOn : Button; 
public var buttonSoundOnSprite : Sprite;
public var buttonSoundOffSprite : Sprite;

function Start(){
	buttonSoundOn.onClick.AddListener(function() { SetSoundSprite();});
	if ((PlayerPrefs.GetInt("SoundState")) == 1 || !PlayerPrefs.HasKey("SoundState")) {
		buttonSoundOn.image.sprite = buttonSoundOnSprite;
    } else {
    	buttonSoundOn.image.sprite = buttonSoundOffSprite;
    }
}

function SetSoundSprite (){
	if (AudioListener.volume != 0) {
		Debug.Log("Off");
		PlayerPrefs.SetInt("SoundState", 0);
		AudioListener.volume = 0;
		buttonSoundOn.image.sprite = buttonSoundOffSprite;
	}else{
		PlayerPrefs.SetInt("SoundState", 1);
		Debug.Log("On");
		AudioListener.volume = 1;
		buttonSoundOn.image.sprite = buttonSoundOnSprite;
	}
}


