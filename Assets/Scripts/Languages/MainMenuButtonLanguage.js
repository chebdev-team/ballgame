﻿#pragma strict
import UnityEngine.UI;

public var engSprite : Sprite;
public var rusSprite : Sprite;

function Start () {
	if(Application.systemLanguage.ToString() == "Russian"){
		gameObject.GetComponent(Button).image.sprite = rusSprite;
	}else{
		gameObject.GetComponent(Button).image.sprite = engSprite;
	}
}

