﻿#pragma strict
import UnityEngine.UI;

public var engString : String;
public var rusString : String;

function Start () {
	if(Application.systemLanguage.ToString() == "Russian"){
		gameObject.GetComponent(Text).text = rusString;
	}else{
		gameObject.GetComponent(Text).text = engString;
	}
}

