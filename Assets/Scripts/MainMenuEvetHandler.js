﻿#pragma strict

public function ShowAdditionMenu (quitPannel: GameObject){
	var buttonPlayPositionY : float;
	buttonPlayPositionY = GameObject.Find ("buttonPlay").transform.position.y;
	iTween.MoveTo (quitPannel, iTween.Hash ("position", new Vector3 (quitPannel.transform.position.x, buttonPlayPositionY, 0.0f), "time", 0.5));
}

public function HideAdditionMenu (quitPannel: GameObject){
	iTween.MoveTo (quitPannel, iTween.Hash ("position", new Vector3 (quitPannel.transform.position.x, quitPannel.transform.position.x + 800f, 0.0f), "time", 0.5));
}

public function ApplicationQuit () {
	Application.Quit();
}

public function LoadLevel (levelName: String) {
	Application.LoadLevel(levelName);
}

public function Tutorial2Step1 () {
	GameObject.Find("Main Camera").GetComponent(Tutorial2).tutorialStart = true ;
	GameObject.Find("Main Camera").GetComponent(Tutorial2).tutorialPlayed0 = true;
}

public function Tutorial2Step3 () {
	GameObject.Find("Main Camera").GetComponent(Tutorial2).isStep3 = true ;
}

public function Tutorial2Step3Fix () {
	GameObject.Find("Main Camera").GetComponent(Tutorial2).isStep3Fix = true ;
}

public function RateUs (){
	#if UNITY_ANDROID
	Application.OpenURL ("http://play.google.com/hedgieball");
	#elif UNITY_IPHONE
	Application.OpenURL ("https://itunes.apple.com/us/app/hedgie-ball/id996121572?mt=8");
	#endif
}