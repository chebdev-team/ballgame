﻿#pragma strict

var ball : GameObject;
var object1 : GameObject;
var objectWithRotation1: GameObject;
private var Xobject1 : float;
private var Yobject1 : float;
private var Xball : float;
private var Yball : float;
private var angleObject1 : float;
private var durakSec : int;
private var buttonSize : float;
var gravityScaleObj1 : float;
var tutorialDrag : boolean = false;
var tutorialRotate : boolean = false;
var tutorialStart : boolean = false;
var tutorialDragOwn : boolean = true;
public var PannelIntro1 :GameObject ;
var PannelIntro1Ypos : float;
var PannelIntro2 :GameObject ;
var pannelIntro2Moved : boolean;
function Start() {
	PannelIntro1Ypos = PannelIntro1.transform.position.y;
    Xball = GameObject.FindWithTag("Player").transform.position.x;
    Yball = GameObject.FindWithTag("Player").transform.position.y;
    buttonSize = Screen.width / 8;
}

function Update() {
    if (Input.GetKeyDown(KeyCode.Escape)) {
        Application.LoadLevel("MainMenu");
    }
}

function OnGUI() {

    if (tutorialDrag && tutorialDragOwn) {
    	if(pannelIntro2Moved == false){
   			iTween.MoveTo (PannelIntro2, iTween.Hash ("position", new Vector3 (PannelIntro2.transform.position.x, PannelIntro1Ypos, 0.0f), "time", 0.5));
    		pannelIntro2Moved = true;
    }

    }
    if (tutorialRotate) {
        Application.LoadLevel("scene22");
    }

}