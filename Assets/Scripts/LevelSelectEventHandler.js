﻿#pragma strict
import UnityEngine.UI;

public function LoadLevel (levelName: String) {
	Application.LoadLevel(levelName);
}

public function HidePanel (panel1 : Canvas){
	 panel1.enabled = false;
}

public function ShowPanel (panel2 : Canvas){
	 panel2.enabled = true;
}

public function Update () {
	if (Input.GetKeyDown(KeyCode.Escape)) {
		  Application.LoadLevel("mainmenu");
	}
}