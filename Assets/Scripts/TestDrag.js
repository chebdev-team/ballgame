﻿#pragma strict
var pickupDistance = 3.0;
var heldObject : GameObject;
var itemHeld = 0;
var objectDist = -.1;
private var hand :GameObject;

function Start() {
    hand = gameObject.FindWithTag("Limit");
}

function Update() {
    if (Input.GetMouseButtonDown(0) && itemHeld == 2) {

        heldObject.transform.parent = null;
        heldObject.GetComponent(Rigidbody2D).isKinematic = false;
        heldObject = null;
        itemHeld = 0;
    }
}

function OnMouseDown() {
    if (itemHeld == 0) {
        heldObject = gameObject;

        heldObject.transform.parent = gameObject.FindWithTag("Limit").transform;
        heldObject.GetComponent(Rigidbody2D).isKinematic = true;
        var handLocation = Vector3(0, objectDist, 0);
        heldObject.transform.localPosition = handLocation;
        itemHeld = 1;
    }
}

function OnMouseUp() {
    if (itemHeld == 1) {
        itemHeld = 2;
    }
}