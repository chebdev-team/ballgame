﻿#pragma strict


var power : int ;
private var isDone : boolean = false;
var buttonSkin : GUISkin;
var twitterSkin : GUISkin;
var fbSkin : GUISkin;
var done0Star: GUISkin;
var done1Star: GUISkin;
var done2Star: GUISkin;
var done3Star: GUISkin;
private var buttonSize : float;
private var buttonIndent : float;
private var timeCollision: float;
private var isAllowed : boolean = true;
var adMob : GameObject;
private var admobTime : float;
private var admobStarted : boolean = false;
private var finishCollisionCount:int = 0;
private var curentLevelScore : int;

/* STARS variables */
var starSkin : GUISkin;
var starEmptySkin : GUISkin;
var timePassed : int;
var limit1 : int;
var limit2 : int;
var limit3 : int;
var limits = new Array(0, 0, 0);
var stars = new Array(1,1,1);
var levelScore : int =0;
/* END STARS variables */

//* Twitter variables */
private var  Address : String = "http://twitter.com/intent/tweet";
private var  lang : String="en";
private var  text : String = "I reached a new level in the Ball For ALL game";
private var  url : String  = "http://play.google.com/ballforall";
/* END Twitter variables */

//* Facebook variables */
private var  AppId : String = "219135384963030";
private var  ShareUrl : String = "http://www.facebook.com/dialog/feed";
private var  fblink : String = "http://ya.ru";
private var  fbpictureLink : String = "https://lh6.googleusercontent.com/-HEC0hzDi1xM/U4sV4yHd9XI/AAAAAAAAABI/icaPXb4rju8/s512-no/512x512.png";
private var  fbname : String = "Ball For ALL achievement";
private var  fbcaption : String = "Find this game on Google Play";
private var  fbdescription : String = "I got new level in Ball for All Game";
private var  fbredirectUri : String = "http://www.facebook.com";
/* END Facebook variables */



function Start() {
    gameObject.GetComponent.<Rigidbody2D>().isKinematic = true;
    limits[0] = limit1;
	limits[1] = limit2;
	limits[2] = limit3;
	curentLevelScore = PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel.ToString());
}

function Update() {
    if ((Time.time - timeCollision) > 1) {
        isAllowed = true;
    }
    if (((Time.time - admobTime) > 3) && admobStarted) {
        isDone = true;
    }
    timePassed = Time.timeSinceLevelLoad;

}

//* Twitter method */
function TwiterShare(text, url, lang) {
    Application.OpenURL(Address +
        "?text=" + WWW.EscapeURL(text) +
        "&url=" + WWW.EscapeURL(url) +
        "&lang=" + WWW.EscapeURL(lang));
}

//Facebook method */
function fbShare(fblink, fbpictureLink, fbname, fbcaption, fbdescription, fbredirectUri) {
    Application.OpenURL(ShareUrl +
        "?app_id=" + AppId +
        "&link=" + WWW.EscapeURL(fblink) +
        "&picture=" + WWW.EscapeURL(fbpictureLink) +
        "&name=" + WWW.EscapeURL(fbname) +
        "&caption=" + WWW.EscapeURL(fbcaption) +
        "&description=" + WWW.EscapeURL(fbdescription) +
        "&redirect_uri=" + WWW.EscapeURL(fbredirectUri));
}


function OnCollisionEnter2D(coll: Collision2D) {
    if (coll.gameObject.name == "Duhovka2" || coll.gameObject.name == "Boy") {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.up * power);
    }


    if (coll.gameObject.name == "Propeller" && isAllowed) {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.up * power);
        timeCollision = Time.time;
        isAllowed = false;
    }


    if (coll.gameObject.name == "Ventilyator") {
        GetComponent.<Rigidbody2D>().AddForce(gameObject.transform.right * power);
    }

    if (coll.gameObject.name == "Basket_bottom") {
        PlayerPrefs.SetInt("Level" + Application.loadedLevel.ToString(), 1);
        (adMob.GetComponent("AdMobPlugin") as MonoBehaviour).enabled = true;
        (adMob.GetComponent("AdMobPluginMockup") as MonoBehaviour).enabled = true;
        admobTime = Time.time;
        admobStarted = true;
        finishCollisionCount++;
        if (finishCollisionCount == 1){
        for(var i = 0; i < stars.length ;i++ ){
        	levelScore += parseInt(stars[i].ToString());
        	
        }
        if(curentLevelScore < levelScore){
         PlayerPrefs.SetInt("LevelScore" + Application.loadedLevel.ToString(), levelScore);
        }
         }
       }
 }

function OnGUI() {
    buttonSkin.button.fontSize = Screen.width * 0.02f;
    if (isDone) {
        GUI.skin = buttonSkin;
        buttonSize = Screen.width / 8;
        buttonIndent = Screen.width / 7;

        GUI.Box(Rect(0, 0, Screen.width, Screen.height), "");

        if (GUI.Button(Rect(((Screen.width / 2) - buttonSize * 2.5), Screen.height / 2, buttonSize, buttonSize), "MENU")) {
            Application.LoadLevel("scene0");
        }

        if (GUI.Button(Rect(((Screen.width / 2) + buttonSize * 1.5), Screen.height / 2, buttonSize, buttonSize), "REPEAT")) {
            Application.LoadLevel(Application.loadedLevel);
        }

        GUI.skin = twitterSkin;
        if (GUI.Button(Rect((Screen.width / 20 * 16.5), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
            TwiterShare(text, url, lang);

        }
        GUI.skin = fbSkin;
        if (GUI.Button(Rect((Screen.width / 20 * 18), (Screen.height / 11.25) * 10, Screen.width / 20, Screen.width / 20), "")) {
            fbShare(fblink, fbpictureLink, fbname, fbcaption, fbdescription, fbredirectUri);
        }
        
        if(PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel) == 0){
			GUI.skin = done0Star;
			GUI.Label(new Rect((Screen.width/2-(Screen.width/3.24)/2),Screen.height/4,Screen.width/3.24,Screen.height/5.62), "");
		}else if (PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel) == 1){
			GUI.skin = done1Star;
			GUI.Label(new Rect((Screen.width/2-(Screen.width/3.24)/2),Screen.height/4,Screen.width/3.24,Screen.height/5.62), "");
		}else if (PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel) == 2){
			GUI.skin = done1Star;
			GUI.Label(new Rect((Screen.width/2-(Screen.width/3.24)/2),Screen.height/4,Screen.width/3.24,Screen.height/5.62), "");
		}else if (PlayerPrefs.GetInt("LevelScore" + Application.loadedLevel) == 3){
			GUI.skin = done3Star;
			GUI.Label(new Rect((Screen.width/2-(Screen.width/3.24)/2),Screen.height/4,Screen.width/3.24,Screen.height/5.62), "");
		}

    }
    if (!isDone) {
    /* STARS LOGIC */
    for(var i = 0; i < limits.length; i++){
		if(limits[i] == timePassed){
			stars[i] = 0;	
		}
		
		if(stars[i] == 1 ){
			GUI.skin = starSkin;
		} else {
			GUI.skin = starEmptySkin;
		}
		
		GUI.Label(new Rect(((Screen.width/9)) + ( i * (Screen.width/50+1) ), (Screen.height/19), Screen.width/50, Screen.width/50), "");
	}
	/* END STARS LOGIC */	
	}
}