﻿#pragma strict

var mySkin : GUISkin;
var tutorialSkin : GUISkin;
var ball : GameObject;
var object1 : GameObject;
var objectWithRotation1: GameObject;
private var Xobject1 : float;
private var Yobject1 : float;
private var Xball : float;
private var Yball : float;
private var angleObject1 : float;
private var durakSec : int;
private var buttonSize : float;
var gravityScaleObj1 : float;
var tutorialDrag : boolean = false;
var tutorialRotate : boolean = false;
public var tutorialStart : boolean = false;
var tutorialFixed : boolean = false;
public var tutorialPlayed0 : boolean = false;
var tutorialPlayed1 : boolean = false;
var tutorialPlayed2 : boolean = false;
private var countPlayPressed : int =0;
private var timePlayPressed1 : float;
private var timePlayPressed2 : float;
public var isStep2 : boolean;
public var isStep3 : boolean;
public var isStep3Fix : boolean;
public var PannelTutorial1 :GameObject ;
var PannelTutorial1Ypos : float;
var PannelTutorial2 :GameObject ;
var PannelTutorial3 :GameObject ;

function Start() {
    Xball = GameObject.FindWithTag("Player").transform.position.x;
    Yball = GameObject.FindWithTag("Player").transform.position.y;
    buttonSize = Screen.width / 8;
    PannelTutorial1Ypos = PannelTutorial1.transform.position.y;
}

function Update() {
    if (Input.GetKeyDown(KeyCode.Escape)) {
        Application.LoadLevel("Scene0");
    }
}

function OnGUI() {

    if (tutorialPlayed1 && (Time.time - timePlayPressed1 > 3)) {
        tutorialPlayed0 = false;
        	iTween.MoveTo (PannelTutorial2, iTween.Hash ("position", new Vector3 (PannelTutorial2.transform.position.x, PannelTutorial1Ypos, 0.0f), "time", 0.5));
            tutorialPlayed1 = false;
            tutorialPlayed2 = true;
            isStep2 =false;
    }


    if (tutorialPlayed0) {
		if(isStep3){
            GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 1;
            durakSec = 1;
            object1.GetComponent.<Rigidbody2D>().gravityScale = gravityScaleObj1;
            object1.GetComponent.<Rigidbody2D>().isKinematic = false;
            ball = GameObject.FindWithTag("Player");
            ball.GetComponent.<Rigidbody2D>().isKinematic = false;
            Xobject1 = object1.transform.position.x;
            Yobject1 = object1.transform.position.y;
            angleObject1 = object1.transform.eulerAngles.z;
            objectWithRotation1.GetComponent. < TutorialRotation2 > ().countDoubleClicks = 0;
            objectWithRotation1.GetComponent. < TutorialRotation2 > ().gravityRotation = false;
            timePlayPressed1 = Time.time;
            tutorialPlayed1 = true;
			isStep3 = false;
			tutorialPlayed0 = false;
        }

        if (isStep3Fix) {
            GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 0;
            GameObject.FindWithTag("Player").transform.position.x = Xball;
            GameObject.FindWithTag("Player").transform.position.y = Yball;
            object1.GetComponent.<Rigidbody2D>().gravityScale = 0;
            ball = GameObject.FindWithTag("Player");
            ball.GetComponent.<Rigidbody2D>().isKinematic = true;
            ball.GetComponent.<Rigidbody2D>().gravityScale = 0;
            object1.GetComponent.<Rigidbody2D>().isKinematic = true;
            if (durakSec != 0) {
                object1.transform.position.x = Xobject1;
                object1.transform.position.y = Yobject1;
                object1.transform.eulerAngles.z = angleObject1;
                objectWithRotation1.GetComponent. < TutorialRotation2 > ().countDoubleClicks = 0;
                objectWithRotation1.GetComponent. < TutorialRotation2 > ().gravityRotation = true;
                tutorialFixed = true;
				isStep3Fix = false;
            }

        }
    }

    if (tutorialPlayed2) {
        if(isStep3) {
            GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 1;
            durakSec = 1;
            object1.GetComponent.<Rigidbody2D>().gravityScale = gravityScaleObj1;
            object1.GetComponent.<Rigidbody2D>().isKinematic = false;
            ball = GameObject.FindWithTag("Player");
            ball.GetComponent.<Rigidbody2D>().isKinematic = false;
            Xobject1 = object1.transform.position.x;
            Yobject1 = object1.transform.position.y;
            angleObject1 = object1.transform.eulerAngles.z;
            objectWithRotation1.GetComponent. < TutorialRotation2 > ().countDoubleClicks = 0;
            objectWithRotation1.GetComponent. < TutorialRotation2 > ().gravityRotation = false;
            countPlayPressed = 2;
            timePlayPressed2 = Time.time;
            isStep3 = false;
        }


         if (isStep3Fix) {
            GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale = 0;
            GameObject.FindWithTag("Player").transform.position.x = Xball;
            GameObject.FindWithTag("Player").transform.position.y = Yball;
            object1.GetComponent.<Rigidbody2D>().gravityScale = 0;
            ball = GameObject.FindWithTag("Player");
            ball.GetComponent.<Rigidbody2D>().isKinematic = true;
            ball.GetComponent.<Rigidbody2D>().gravityScale = 0;
            object1.GetComponent.<Rigidbody2D>().isKinematic = true;
            if (durakSec != 0) {
                object1.transform.position.x = Xobject1;
                object1.transform.position.y = Yobject1;
                object1.transform.eulerAngles.z = angleObject1;
                objectWithRotation1.GetComponent. < TutorialRotation2 > ().countDoubleClicks = 0;
                objectWithRotation1.GetComponent. < TutorialRotation2 > ().gravityRotation = true;
                tutorialFixed = true;
                isStep3Fix = false;
            }
        }
    }

    if (tutorialFixed && countPlayPressed == 2 && (Time.time - timePlayPressed2 > 6)) {
        if (tutorialDrag || tutorialRotate) {
			iTween.MoveTo (PannelTutorial3, iTween.Hash ("position", new Vector3 (PannelTutorial3.transform.position.x, PannelTutorial1Ypos, 0.0f), "time", 0.5));
        }
    }
}