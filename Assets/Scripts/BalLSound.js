﻿#pragma strict
var bounceMain: AudioClip;
var bounce2: AudioClip;
var bounce3: AudioClip;
var bounceFinish: AudioClip;
var bounceCount: int = 0;
var isFinish: int = 0;


function Start() {

}


function FixedUpdate() {
    if (GameObject.FindWithTag("Player").GetComponent.<Rigidbody2D>().gravityScale == 0) {
        bounceCount = 0;
    }
}


function OnCollisionEnter2D(coll: Collision2D) {

    if (coll.gameObject.name == "Basket_bottom") {
        isFinish += 1;
    }

    if (isFinish == 1) {
        bounceCount = 0;
        GetComponent.<AudioSource>().clip = bounceFinish;
        GetComponent.<AudioSource>().Play();
    }

    if (coll.gameObject) {
        bounceCount += 1;
    }

    if (bounceCount % 2 == 0) {
        if (!GetComponent.<AudioSource>().isPlaying) {
            GetComponent.<AudioSource>().clip = bounce3;
            GetComponent.<AudioSource>().Play();
        }
    } else {
        if (!GetComponent.<AudioSource>().isPlaying) {
            GetComponent.<AudioSource>().clip = bounceMain;
            GetComponent.<AudioSource>().Play();
        }


    }
}